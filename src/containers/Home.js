import React, { Component } from 'react';
import logo from '../logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css'
import '../App.css';
import PriceList from '../components/PriceList';
import ViewTab from '../components/ViewTab';
import { LIST_VIEW, CHART_VIEW, TYPE_OUTCOME, parseToYearAndMonth,padLeft } from '../utility'
import MonthPicker from '../components/MonthPicker'
import TotalPrice from '../components/TotalPrice';
import CreateBtn from '../components/CreateBtn';
import PriceFrom from '../components/PriceForm';
import { Tabs, Tab } from '../components/Tabs';

const categorys = {
    "1":{
        "id": 1,
        "name": "娱乐",
        "type": "outcome",
        "iconName": 'ios-plane'
    },
    "2":{
      "id": 2,
      "name": "食",
      "type": "outcome",
      "iconName": "ios-alert"
    },
    "3":{
        "id": 3,
        "name": "副业",
        "type": "income",
        "iconName": "md-calculator"
    }
  }


const items = [
    {
      "id":1,
      "title": "嫖",
      "price": 500,
      "date": "2020-0302",
      "cid": "1"
    },
    {
      "id":2,
      "title": "吃",
      "price": 320,
      "date": "2020-0402",
      "cid": "2"
    },
    {
      "id":3,
      "title": "炒股",
      "price": 320000,
      "date": "2020-0202",
      "cid": "3"
    },
  ]
const newItem = {
    "id":20,
    "title": "吃ccc",
    "price": 320,
    "date": "2020-0102",
    "cid": "2"
}

class Home extends Component {
    constructor(prope){
      super(prope)
      this.state = {
        items,
        currentDate: parseToYearAndMonth(),
        tabView:LIST_VIEW,
      }
    }
    queryMonthDate = (year, month) =>{
      this.setState({
        currentDate: {year, month}
      })
    }
    switchView = (view)=>{
      this.setState({
        tabView:view
      })
    }
    createItem = () =>{
        this.setState({
          items : [newItem, ...this.state.items]
        })
    }

    modifyItem = (modifyedItem) =>{
      const modifiedItems = this.state.items.map(item =>{
        if(item.id == modifyedItem.id){
          return {...item, title: '更新后的标题'}
        }else{
          return item
        }
      })
      this.setState({
        items: modifiedItems
      })
    }

    deleteItem = (deletedItem)=>{
      const resout = this.state.items.filter(item => item.id !== deletedItem.id)
      this.setState({
        items : resout
      })
    }

    render(){
      const { items, currentDate, tabView } = this.state  
      const itemsWithCategory = items.map(item =>{
        item.category = categorys[item.cid]
        return item
      }).filter(item => {
        return item.date.includes(`${currentDate.year}-${padLeft(currentDate.month)}`)
      })
      console.log(itemsWithCategory)
      let totalIncome = 0, totalOutcome = 0
      itemsWithCategory.forEach(item =>{
        if(item.category.type === TYPE_OUTCOME){
          totalOutcome += item.price
        }else{
          totalIncome += item.price
        }
      })

      return(
          <React.Fragment>
              <header className="App-header">
                  
                  {/* <p>
                  Edit <code>src/App.js</code> and save to reload.
                  </p>
                  <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
                  Learn React
                  </a> */}
                  <div className="row mb-5">
                    <img src={logo} className="App-logo" alt="logo" />
                  </div>
                  <div className="row">
                    <div className="col">
                      <MonthPicker year={currentDate.year} month={currentDate.month} onChange={this.queryMonthDate}/>
                    </div>
                    <div className="col">
                      <TotalPrice income={totalIncome} outcome={totalOutcome}/>
                    </div>
                  </div>
              </header>
              <div className="content-area py-3 px-3">
                <Tabs >
                  <Tab>第一个tab</Tab>
                  <Tab>第二个tab</Tab>
                </Tabs>
                <ViewTab activeTab = {tabView} onTabChange = {this.switchView}/>
                <CreateBtn onClick={this.createItem}/>
                { tabView === LIST_VIEW &&
                    <PriceList 
                    items={itemsWithCategory} 
                    onDeleteItem={this.deleteItem} 
                    onModifyItem={this.modifyItem}/>
                }
                { tabView === CHART_VIEW &&
                  <h2>图表显示</h2>
                }
                
              </div>
              
              <PriceFrom onFormSubmit={()=>{}} onCancelSubmit={()=>{}} item={{}}></PriceFrom>
          </React.Fragment>
      )
    }
}

export default Home