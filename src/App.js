import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import Home from './containers/Home'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Create from './containers/Create';

/*function App() {
  const items = [
    {
      "id":1,
      "title": "嫖",
      "price": 500,
      "date": "2020-0202",
      "category": {
        "id": 1,
        "name": "娱乐",
        "type": "outcome",
        "iconName": "ios-plane"
      }
    },
    {
      "id":2,
      "title": "吃",
      "price": 320,
      "date": "2020-0202",
      "category": {
        "id": 1,
        "name": "娱乐",
        "type": "outcome",
        "iconName": "game-controller-outline"
      }
    },
    {
      "id":3,
      "title": "炒股",
      "price": 320000,
      "date": "2020-0202",
      "category": {
        "id": 2,
        "name": "副业",
        "type": "income",
        "iconName": "bar-chart-outline"
      }
    },
  ]
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <PriceList 
      items={items} 
      onDeleteItem={(item)=>{alert(item.id)}} 
      onModifyItem={(item)=>{alert(item.id)}}/>
    </div>
  );
}*/
const items = [
  {
    "id":1,
    "title": "嫖",
    "price": 500,
    "date": "2020-0202",
    "category": {
      "id": 1,
      "name": "娱乐",
      "type": "outcome",
      "iconName": 'ios-plane'
    }
  },
  {
    "id":2,
    "title": "吃",
    "price": 320,
    "date": "2020-0202",
    "category": {
      "id": 1,
      "name": "娱乐",
      "type": "outcome",
      "iconName": "ios-alert"
    }
  },
  {
    "id":3,
    "title": "炒股",
    "price": 320000,
    "date": "2020-0202",
    "category": {
      "id": 2,
      "name": "副业",
      "type": "income",
      "iconName": "md-calculator"
    }
  },
]

class App extends Component {
    render() {
      return(
        <Router>
          <div className='App'>
            <ul>
              <Link to="/">Home</Link>
              <Link to="/create">Create</Link>
              <Link to="/edit/11">Edit</Link>
            </ul>
            <Route path="/" exact component={Home} />
            <Route path="/create" component={Create} />
            <Route path="/edit/:id" component={Create} />
            {/* <Home /> */}
          </div>
        </Router>
      )
    }
}
export default App;
