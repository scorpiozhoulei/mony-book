/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import Ionicon from 'react-ionicons'
import PropTypes from 'prop-types'
import { padLeft, range } from '../utility'

class MonthPicker extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            isOpen: false,
            selectedYear: this.props.year
        }
    }
    //实现点击其它地方，把月份隐藏。实现逻辑。对docmement进行点击事件监听。并移除年份部分
    componentDidMount(){
        document.addEventListener('click',this.handleClick,false)
    }
    componentWillMount(){
        document.removeEventListener('click',this.handleClick,false)
    }

    handleClick = (event) =>{
        if(this.node.contains(event.target)) return;
        this.setState({
            isOpen: false
        })
    }

    toggleDropdown = (event) =>{
        event.preventDefault()
        this.setState({
            isOpen: !this.state.isOpen,
        })
    }
    selectYear = (event,yearNumber) =>{
        event.preventDefault()
         this.setState({
             selectedYear: yearNumber
         })
    }
    selectMonth = (event, monthNumber) =>{
        event.preventDefault()
        this.setState({
            isOpen: false,
        })
        this.props.onChange(this.state.selectedYear,monthNumber)
    }

    render(){
        const { year, month } = this.props
        const { isOpen,selectedYear } = this.state
        const monthRange = range(12,1)
        console.log(monthRange)
        const yearRange = range(9, -4).map(number => number + year)
        console.log(yearRange)
        const generateSelectClass = (curr, view) => curr === view ? "dropdown-item active" : "dropdown-item"

        return (
            <div className="dropdown month-picker-component" ref={(ref) =>{ this.node = ref }}>
                <h4>选择月份</h4>
                <button 
                    className="btn btn-lg btn-secondary dropdown-toggle"
                    onClick={this.toggleDropdown}
                >
                    {`${year}年 ${ month }月`}
                </button>
                {
                    isOpen &&
                    <div className="dropdown-menu" style={{display: 'block'}}>
                        <div className="row">
                            <div className="col border-right">
                                { yearRange.map((yearNumber, index) =>
                                    <a  key={index} 
                                        className={generateSelectClass(selectedYear,yearNumber)}
                                        href="#"
                                        onClick={(event) => {this.selectYear(event,yearNumber)}}
                                    >
                                        {yearNumber} 年
                                    </a>
                                )}
                            </div>
                            <div className="col">
                                { monthRange.map((monthNumber, index) =>
                                    <a key={index} href="#" className={ generateSelectClass(month,monthNumber) }
                                        onClick={(event) => {this.selectMonth(event,monthNumber)}}
                                    >
                                        {padLeft(monthNumber)} 月
                                    </a>
                                )}
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
    }
}

export default MonthPicker