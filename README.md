This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

## 前置概念
### 展示型组件
* 关心数据的展示方方式
* 不依赖app中的其它文件
* 不关心数据是如何加载和变化的
* 仅通过props接收数据和回调函数
* 除非需要用到state，生命周期函数或性能优化，通常写成函数式组件
### 容器型组件
* 关心数据的运作方式
* 为展示型组件提供数据和操作数据的方法
* 为展示型组件提供回调函数
* 通常是有状态的，并且作为数据源存在

## 文件结构及命名规范

* components 文件夹存放所有展示型组件
* containers 文件夹存放所有容器型组件
* 两文件夹下的__tets__ 文件夹存放多有组件的测试文件
* 文件使用Pascal Case命名发
* 以上统一放在src下


## React-Router开发SPA应用

### 什么是SPA？

* 字面义单页面web应用程序或网站
* 在和用户交互的时候不会跳转到其它页面
* 用js实现url变换和动态的变换html的内容

### SPA应用的优点

* 速度快
* 体验好
* 前后端分离

### React Router

* [React-Router地址仓库](https://github.com/ReactTraining/react-router)

* [React-Router官网地址](https://reacttraining.com/react-router/)

### React Router 特点

* Components Based（基于组件）
* 声明式和可组合
* 支持多种应用

